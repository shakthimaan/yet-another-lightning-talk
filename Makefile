LIST =  yet-another-lightning-talk \
	yet-another-lightning-talk-season-two

all:
	for file in $(LIST); do \
		pdflatex $$file; \
	done

clean:
	rm -f *.aux *.log *.nav *.out *.pdf *.snm *.toc *.vrb *~
